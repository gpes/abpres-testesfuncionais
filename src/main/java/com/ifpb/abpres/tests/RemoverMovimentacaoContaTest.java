package com.ifpb.abpres.tests;

import org.junit.Assert;
import org.junit.Test;

import com.ifpb.abpres.core.BaseTest;
import com.ifpb.abpres.core.Propriedades;
import com.ifpb.abpres.pages.ContasPage;
import com.ifpb.abpres.pages.MenuPage;

public class RemoverMovimentacaoContaTest extends BaseTest {
	
	MenuPage menuPage = new MenuPage();
	ContasPage contasPage = new ContasPage();

	@Test
	public void testExcluirContaComMovimentacao(){
		menuPage.acessarTelaListarConta();
		
		contasPage.clicarExcluirConta(Propriedades.NOME_CONTA_ALTERADA);
		
		Assert.assertEquals("Conta em uso na movimentações", contasPage.obterMensagemErro());
	}

}
