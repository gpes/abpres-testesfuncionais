package com.ifpb.abpres.tests;

import org.junit.Assert;
import org.junit.Test;

import com.ifpb.abpres.core.BaseTest;
import com.ifpb.abpres.core.Propriedades;
import com.ifpb.abpres.pages.HomePage;
import com.ifpb.abpres.pages.MenuPage;

public class SaldoTest extends BaseTest {
	HomePage page = new HomePage();
	MenuPage menu = new MenuPage();

	@Test
	public void testSaldoConta(){
		menu.acessarTelaPrincipal();
		Assert.assertEquals("500.00", page.obterSaldoConta(Propriedades.NOME_CONTA_ALTERADA));
	}
}