# GPES testesFuncionais
>>Roteiro de testes

Planejamento e controle

Selecionar as condições de testes(itens de teste)

Modelar os casos de testes

Executar o teste 

Verificar resultados

Avaliar os critérios de conclusão

Relatar o ocorrido

Encerrar as atividade de testes

Verificar resultado

Avaliar

# Montando o Ambiente

Java 8

Eclipse IDE

Baixar Drivers do Selenium

Firefox =>Baixar Gecko Driver para seu sistema operacional:

https://github.com/mozilla/geckodriver/releases ou

Chrome => https://sites.google.com/a/chromium.org/chromedriver/downloads

Basta baixar e descompactar, para então usar o respectivo local do arquivo para os testes.

O ideal é colocar esse local no path do sistema operacional, com variáveis de ambiente, no Windows, por exemplo.

#FrameWork de teste:

1 - Driver Centralizado [ DriverFactory ]

2 - Herança de Comportamentos [PageObject]

3 - Reúso do Browser [anotações @before e @after]

4 - Screenshot ao final dos testes

5 - Chaveamento para outros browsers[suíte de testes]

6 - Padronização [semelhante a facade, fachada]

#Como implementar
Para cada URI haverá uma classe de testes, que usará outra classe com métodos do Selenium,  
para checar as funcionalidade do sistema. 

Definir quais URIs serão testas, por exemplo:

arquivos

config

exception

praticas

security

usuários

No Eclipse, crie >>projeto>> Other >> Maven >> Maven Project, em seguida marque a opção 

para criar projeto simples.

Ao final, informar um group id, artifact id e então finalizar.

driver.get(file:///" + System.getProperty("user.dir") + "/src/main/resources/nome do arquivo.

Sendo possível uso de URI remota, basta trocar pela URI

Para localizar campos nas páginas use o chrome, em ferramentas, mais ferramentas, 

ferramentas do desenvolvedor ou ctrl + shift + i

É selecionar o trecho do código a ser localizado e usar o comando ctrl + f, então escolher 

qual o tipo de seleção: por string, selector ou Xpath



